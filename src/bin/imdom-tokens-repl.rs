use imdom::xml_1_0;
use std::io;
use std::io::Write;

fn main() {
    println!("Immutable DOM Tokens REPL\n");

    println!("Exit the REPL by entering 'exit'.\n");

    loop {
        print!("> ");
        io::stdout()
            .flush()
            .ok()
            .expect("Error: Unable to flush stdout.");

        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(_n) => {
                if input == "exit\n" {
                    break;
                }

                match xml_1_0::tokenize(input) {
                    Ok(token_list) => println!("{:?}", token_list),
                    Err(tokenize_error) => {
                        println!("{}", tokenize_error.get_message());
                    }
                }
            }
            Err(error) => println!("Error reading input: {}", error),
        }
    }
}
