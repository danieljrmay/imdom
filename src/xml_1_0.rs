#[derive(Copy, Clone, Debug, PartialEq)]
enum Mode {
    Document,
    Prolog,
    Element,
    Misc,
    XMLDecl,
    VersionInfo,
    Whitespace(Quantifier),
    Eq,
    VersionNum,
    EncName,
    QuoteStart,
    QuoteEnd(QuoteStyle),
    YesNo,
    Comment,
    PI,
    PITarget,
}

#[derive(Copy, Clone, Debug, PartialEq)]
enum Quantifier {
    ZeroOrOne,  // ?
    ZeroOrMore, // *
    OneOrMore,  // +
}

#[derive(Copy, Clone, Debug, PartialEq)]
enum QuoteStyle {
    Single,
    Double,
}

#[derive(Debug)]
struct ModeStack {
    stack: Vec<Mode>,
}
impl ModeStack {
    fn new() -> ModeStack {
        ModeStack {
            stack: vec![Mode::Document, Mode::Misc, Mode::Element, Mode::Prolog],
        }
    }

    fn peek(&self) -> Mode {
        let peek_index = self.stack.len() - 1;
        self.stack[peek_index].clone()
    }

    fn pop(&mut self) -> Mode {
        self.stack.pop().expect("Error popping ModeStack.")
    }

    fn push(&mut self, mode: Mode) {
        self.stack.push(mode);
    }

    fn replace(&mut self, mode: Mode) {
        self.stack.pop();
        self.stack.push(mode);
    }
}

#[cfg(test)]
mod mode_stack_tests {
    use super::*;

    #[test]
    fn mode_stack() {
        let mut stack = ModeStack::new();
        assert_eq!(stack.peek(), Mode::Prolog);
        assert_eq!(stack.peek(), Mode::Prolog);
        stack.push(Mode::XMLDecl);
        assert_eq!(stack.peek(), Mode::XMLDecl);
        assert_eq!(stack.pop(), Mode::XMLDecl);
        assert_eq!(stack.peek(), Mode::Prolog);
    }
}

#[derive(Debug, PartialEq)]
enum Token {
    XMLDeclStart,
    XMLVersion(XMLVersionNumber),
    XMLEncoding(EncName),
    XMLStandalone(bool),
    XMLDeclEnd,
    Comment(Comment),
    PIStart,
    PITarget(PITarget),
    PIData(PIData),
    PIEnd,
}

#[derive(Debug, PartialEq)]
enum XMLVersionNumber {
    OnePointZero,
    OnePointOne,
}

// [81]
#[derive(Debug, PartialEq)]
struct EncName {
    enc_name: String,
}
impl EncName {
    fn new(enc_name: String) -> EncName {
        EncName { enc_name }
    }
}

// TODO [15]
#[derive(Debug, PartialEq)]
struct Comment {
    comment: String,
}
impl Comment {
    fn new(comment: String) -> Comment {
        Comment { comment }
    }
}
// TODO [16]
#[derive(Debug, PartialEq)]
struct PI {
    target: PITarget,
    data: PIData,
}

// TODO [17]
#[derive(Debug, PartialEq)]
struct PITarget {
    target: String,
}
impl PITarget {
    fn new(target: String) -> PITarget {
        PITarget { target }
    }
}

#[derive(Debug, PartialEq)]
struct PIData {
    data: String,
}
impl PIData {
    fn new(data: String) -> PIData {
        PIData { data }
    }
}

#[derive(Debug, PartialEq)]
pub struct TokenList {
    tokens: Vec<Token>,
}
impl TokenList {
    fn new() -> TokenList {
        TokenList { tokens: Vec::new() }
    }
    fn new_from_tokens(tokens: Vec<Token>) -> TokenList {
        TokenList { tokens }
    }
    fn push(&mut self, token: Token) {
        &self.tokens.push(token);
    }
}

pub struct TokenizeError {
    tokens: TokenList,
    xml: String,
    c: Vec<char>,
    i: usize,
    mode_stack: ModeStack,
    message: String,
}
impl TokenizeError {
    fn new(
        tokens: TokenList,
        xml: String,
        c: Vec<char>,
        i: usize,
        mode_stack: ModeStack,
        message: String,
    ) -> TokenizeError {
        TokenizeError {
            tokens,
            xml,
            c,
            i,
            mode_stack,
            message,
        }
    }

    pub fn get_message(&self) -> String {
        let around_start;
        if self.i < 5 {
            around_start = 0;
        } else {
            around_start = self.i - 5;
        }

        let around_end;
        if self.i + 5 >= self.xml.len() {
            around_end = self.xml.len() - 1;
        } else {
            around_end = self.i + 5;
        }
        let around = &self.xml[around_start..around_end];

        String::from(format!(
            "Tokenizing error: {}\ntokens={:?}\nmode_stack={:?}\nc[{}]='{}'\nAround=\"{}\"",
            self.message, self.tokens, self.mode_stack, self.i, self.c[self.i], around
        ))
    }
}

pub fn tokenize(xml: String) -> Result<TokenList, TokenizeError> {
    let mut tokens = TokenList::new();
    let mut mode_stack = ModeStack::new();
    let c: Vec<char> = xml.chars().collect();
    let length = c.len();
    let mut i = 0;

    while i < length {
        match mode_stack.peek() {
            Mode::Document => {
                return Err(TokenizeError::new(
                    tokens,
                    xml,
                    c,
                    i,
                    mode_stack,
                    format!("Mode::Document not implemented."),
                ));
            }
            Mode::Prolog => {
                // Frankie Howe - "The Prologue..."
                if i == 0 && c[0] == '<' && c[1] == '?' && c[2] == 'x' && c[3] == 'm' && c[4] == 'l'
                {
                    i += 5;
                    tokens.push(Token::XMLDeclStart);
                    mode_stack.push(Mode::XMLDecl);
                    mode_stack.push(Mode::VersionInfo);
                    mode_stack.push(Mode::Whitespace(Quantifier::OneOrMore));
                } else if c[i] == '<' && c[i + 1] == '!' && c[i + 2] == '-' && c[i + 3] == '-' {
                    i += 4;
                    mode_stack.push(Mode::Comment);
                } else if c[i] == '<' && c[i + 1] == '?' {
                    i += 2;
                    tokens.push(Token::PIStart);
                    mode_stack.push(Mode::PI);
                    mode_stack.push(Mode::PITarget);
                } else if is_whitespace(c[i]) {
                    i += 1;
                    mode_stack.push(Mode::Whitespace(Quantifier::ZeroOrMore));
                } else if c[i] == '<'
                    && c[i + 1] == '!'
                    && c[i + 2] == 'D'
                    && c[i + 3] == 'O'
                    && c[i + 4] == 'C'
                    && c[i + 5] == 'T'
                    && c[i + 6] == 'Y'
                    && c[i + 7] == 'P'
                    && c[i + 7] == 'E'
                {
                    //TODO check length before lookahead
                    return Err(TokenizeError::new(
                        tokens,
                        xml,
                        c,
                        i,
                        mode_stack,
                        format!("Doctype mode not implemented yet."),
                    ));
                } else {
                    return Err(TokenizeError::new(
                        tokens,
                        xml,
                        c,
                        i,
                        mode_stack,
                        format!("Unexpected character in Prolog."),
                    ));
                }
            }
            Mode::Element => {
                return Err(TokenizeError::new(
                    tokens,
                    xml,
                    c,
                    i,
                    mode_stack,
                    format!("Mode::Element not implemented."),
                ));
            }
            Mode::Misc => {
                //if comment() || pi() || s() {
                // processing
                // } else {
                // pop
                //}
                // detect_comment
                // detect_pi
                // detect_s
                return Err(TokenizeError::new(
                    tokens,
                    xml,
                    c,
                    i,
                    mode_stack,
                    format!("Mode::Misc not implemented."),
                ));
            }
            Mode::XMLDecl => {
                if is_whitespace(c[i]) {
                    i += 1;
                } else if c[i] == '?' && c[i + 1] == '>' {
                    i += 2;
                    tokens.push(Token::XMLDeclEnd);
                    mode_stack.pop();
                } else if c[i] == 'e'
                    && c[i + 1] == 'n'
                    && c[i + 2] == 'c'
                    && c[i + 3] == 'o'
                    && c[i + 4] == 'd'
                    && c[i + 5] == 'i'
                    && c[i + 6] == 'n'
                    && c[i + 7] == 'g'
                {
                    i += 8;
                    mode_stack.push(Mode::EncName);
                    mode_stack.push(Mode::QuoteStart);
                    mode_stack.push(Mode::Whitespace(Quantifier::ZeroOrMore));
                    mode_stack.push(Mode::Eq);
                    mode_stack.push(Mode::Whitespace(Quantifier::ZeroOrMore));
                } else if c[i] == 's'
                    && c[i + 1] == 't'
                    && c[i + 2] == 'a'
                    && c[i + 3] == 'n'
                    && c[i + 4] == 'd'
                    && c[i + 5] == 'a'
                    && c[i + 6] == 'l'
                    && c[i + 7] == 'o'
                    && c[i + 8] == 'n'
                    && c[i + 9] == 'e'
                {
                    i += 10;
                    mode_stack.push(Mode::YesNo);
                    mode_stack.push(Mode::QuoteStart);
                    mode_stack.push(Mode::Whitespace(Quantifier::ZeroOrMore));
                    mode_stack.push(Mode::Eq);
                    mode_stack.push(Mode::Whitespace(Quantifier::ZeroOrMore));
                } else {
                    return Err(TokenizeError::new(
                        tokens,
                        xml,
                        c,
                        i,
                        mode_stack,
                        format!("Unexpected character in XMLDecl."),
                    ));
                }
            }
            Mode::VersionInfo => {
                if c[i] == 'v'
                    && c[i + 1] == 'e'
                    && c[i + 2] == 'r'
                    && c[i + 3] == 's'
                    && c[i + 4] == 'i'
                    && c[i + 5] == 'o'
                    && c[i + 6] == 'n'
                {
                    i += 7;
                    mode_stack.pop();
                    mode_stack.push(Mode::VersionNum);
                    mode_stack.push(Mode::QuoteStart);
                    mode_stack.push(Mode::Whitespace(Quantifier::ZeroOrMore));
                    mode_stack.push(Mode::Eq);
                    mode_stack.push(Mode::Whitespace(Quantifier::ZeroOrMore));
                } else {
                    return Err(TokenizeError::new(
                        tokens,
                        xml,
                        c,
                        i,
                        mode_stack,
                        format!("Expected XML declaration version attribute."),
                    ));
                }
            }
            Mode::Whitespace(quantifier) => match quantifier {
                Quantifier::OneOrMore => {
                    if is_whitespace(c[i]) {
                        i += 1;
                        mode_stack.replace(Mode::Whitespace(Quantifier::ZeroOrMore));
                    } else {
                        return Err(TokenizeError::new(
                            tokens,
                            xml,
                            c,
                            i,
                            mode_stack,
                            format!("Expected one or more whitespace characters."),
                        ));
                    }
                }
                Quantifier::ZeroOrMore => {
                    while is_whitespace(c[i]) {
                        // TODO fix for i >= length
                        i += 1;
                    }
                    mode_stack.pop();
                }
                Quantifier::ZeroOrOne => {
                    if is_whitespace(c[i]) {
                        i += 1;
                    }
                    mode_stack.pop();
                }
            },
            Mode::Eq => {
                if c[i] == '=' {
                    i += 1;
                    mode_stack.pop();
                } else {
                    return Err(TokenizeError::new(
                        tokens,
                        xml,
                        c,
                        i,
                        mode_stack,
                        format!("Expected an '=' character."),
                    ));
                }
            }
            Mode::VersionNum => {
                if c[i] == '1' && c[i + 1] == '.' && c[i + 2] == '0' {
                    i += 3;
                    tokens.push(Token::XMLVersion(XMLVersionNumber::OnePointZero));
                    mode_stack.pop();
                } else {
                    return Err(TokenizeError::new(
                        tokens,
                        xml,
                        c,
                        i,
                        mode_stack,
                        format!("Expected XML version 1.0."),
                    ));
                }
            }
            Mode::EncName => {
                if is_enc_name_start_char(c[i]) {
                    let enc_name_start = i;
                    i += 1;

                    while is_enc_name_char(c[i]) {
                        // TODO fix for i >= length
                        i += 1;
                    }

                    let enc_name_end = i;
                    tokens.push(Token::XMLEncoding(EncName::new(
                        c[enc_name_start..enc_name_end].iter().collect(),
                    )));
                    mode_stack.pop();
                } else {
                    return Err(TokenizeError::new(
                        tokens,
                        xml,
                        c,
                        i,
                        mode_stack,
                        format!("Unexpected encoding name start character."),
                    ));
                }
            }
            Mode::QuoteStart => {
                if c[i] == '"' {
                    i += 1;
                    mode_stack.pop();
                    let quoted_content_mode = mode_stack.pop();
                    mode_stack.push(Mode::QuoteEnd(QuoteStyle::Double));
                    mode_stack.push(quoted_content_mode);
                } else if c[i] == '\'' {
                    i += 1;
                    mode_stack.pop();
                    let quoted_content_mode = mode_stack.pop();
                    mode_stack.push(Mode::QuoteEnd(QuoteStyle::Single));
                    mode_stack.push(quoted_content_mode);
                } else {
                    return Err(TokenizeError::new(
                        tokens,
                        xml,
                        c,
                        i,
                        mode_stack,
                        format!("Expected an opening single or double quote."),
                    ));
                }
            }
            Mode::QuoteEnd(quote_style) => match quote_style {
                QuoteStyle::Single => {
                    if c[i] == '\'' {
                        i += 1;
                        mode_stack.pop();
                    } else {
                        return Err(TokenizeError::new(
                            tokens,
                            xml,
                            c,
                            i,
                            mode_stack,
                            format!("Expected a closing single quote."),
                        ));
                    }
                }
                QuoteStyle::Double => {
                    if c[i] == '"' {
                        i += 1;
                        mode_stack.pop();
                    } else {
                        return Err(TokenizeError::new(
                            tokens,
                            xml,
                            c,
                            i,
                            mode_stack,
                            format!("Expected a closing double quote."),
                        ));
                    }
                }
            },
            Mode::YesNo => {
                if c[i] == 'y' && c[i + 1] == 'e' && c[i + 2] == 's' {
                    i += 3;
                    tokens.push(Token::XMLStandalone(true));
                    mode_stack.pop();
                } else if c[i] == 'n' && c[i + 1] == 'o' {
                    i += 2;
                    tokens.push(Token::XMLStandalone(false));
                    mode_stack.pop();
                } else {
                    return Err(TokenizeError::new(
                        tokens,
                        xml,
                        c,
                        i,
                        mode_stack,
                        format!("Unexpected character expecting \"yes\" or \"no\"."),
                    ));
                }
            }
            Mode::Comment => {
                if is_comment_start_char(c[i]) {
                    let comment_start = i;
                    let comment_end;
                    i += 1;

                    while i < length - 2 {
                        if c[i] == '-' && c[i + 1] == '-' && c[i + 2] == '>' {
                            comment_end = i;
                            i += 3;
                            tokens.push(Token::Comment(Comment::new(
                                c[comment_start..comment_end].iter().collect(),
                            )));
                            mode_stack.pop();
                            break;
                        } else if c[i] == '-' && c[i + 1] == '-' {
                            return Err(TokenizeError::new(
                                tokens,
                                xml,
                                c,
                                i,
                                mode_stack,
                                format!("Illegal consecutive '-' characters in comment."),
                            ));
                        } else if is_char(c[i]) {
                            i += 1;
                        } else {
                            return Err(TokenizeError::new(
                                tokens,
                                xml,
                                c,
                                i,
                                mode_stack,
                                format!("Illegal character in comment."),
                            ));
                        }
                    }

                    if mode_stack.peek() == Mode::Comment {
                        return Err(TokenizeError::new(
                            tokens,
                            xml,
                            c,
                            i,
                            mode_stack,
                            format!("Syntax error reached end of input before closing comment."),
                        ));
                    }
                } else {
                    return Err(TokenizeError::new(
                        tokens,
                        xml,
                        c,
                        i,
                        mode_stack,
                        format!("Unexpected first character of comment."),
                    ));
                }
            }
            Mode::PI => {
                if c[i] == '?' && c[i + 1] == '>' {
                    //TODO handle case of i+1 >= length in above if. Duplicate elsewhere
                    i += 2;
                    tokens.push(Token::PIData(PIData::new(String::new())));
                    tokens.push(Token::PIEnd);
                    mode_stack.pop();
                } else if is_whitespace(c[i]) {
                    let data_start = i;

                    while i < length - 1 {
                        if c[i] == '?' && c[i + 1] == '>' {
                            let data_end = i;
                            i += 2;
                            tokens.push(Token::PIData(PIData::new(
                                c[data_start..data_end].iter().collect(),
                            )));
                            tokens.push(Token::PIEnd);
                            mode_stack.pop();
                            break;
                        } else if is_char(c[i]) {
                            i += 1;
                        } else {
                            return Err(TokenizeError::new(
                                tokens,
                                xml,
                                c,
                                i,
                                mode_stack,
                                format!("Illegal character in processing instruction data."),
                            ));
                        }
                    }
                } else {
                    return Err(TokenizeError::new(
                        tokens,
                        xml,
                        c,
                        i,
                        mode_stack,
                        format!("The first character after a PI target must be whitespace."),
                    ));
                }
            }
            Mode::PITarget => {
                if is_name_start_char(c[i]) {
                    let name_start = i;
                    i += 1;

                    while i < length {
                        if is_name_char(c[i]) {
                            i += 1;
                        } else {
                            let name_end = i;
                            let name: String = c[name_start..name_end].iter().collect();

                            if &name.to_lowercase() == "xml" {
                                return Err(TokenizeError::new(
                                    tokens,
                                    xml,
                                    c,
                                    i,
                                    mode_stack,
                                    format!("The processing instruction target names 'xml', 'XML', etc. are reserved."),
                                ));
                            }

                            tokens.push(Token::PITarget(PITarget::new(
                                c[name_start..name_end].iter().collect(),
                            )));
                            mode_stack.pop();
                            break;
                        }
                    }
                } else {
                    return Err(TokenizeError::new(
                        tokens,
                        xml,
                        c,
                        i,
                        mode_stack,
                        format!("Unexpected first character of comment."),
                    ));
                }
            }
        }
    }

    Ok(tokens)
}

#[cfg(test)]
mod tokenize_tests {
    use super::*;

    fn assert_xml_generates_tokens(xml: &str, expected_tokens: Vec<Token>) {
        let result = tokenize(xml.to_string());
        match result {
            Ok(tokens) => assert_eq!(tokens, TokenList::new_from_tokens(expected_tokens)),
            Err(error) => panic!(error.get_message()),
        }
    }

    #[test]
    fn xml_declaration() {
        assert_xml_generates_tokens(
            "<?xml version='1.0'?>",
            vec![
                Token::XMLDeclStart,
                Token::XMLVersion(XMLVersionNumber::OnePointZero),
                Token::XMLDeclEnd,
            ],
        );

        assert_xml_generates_tokens(
            "<?xml version=\"1.0\"?>",
            vec![
                Token::XMLDeclStart,
                Token::XMLVersion(XMLVersionNumber::OnePointZero),
                Token::XMLDeclEnd,
            ],
        );

        assert_xml_generates_tokens(
            "<?xml      version               =  '1.0'     ?>",
            vec![
                Token::XMLDeclStart,
                Token::XMLVersion(XMLVersionNumber::OnePointZero),
                Token::XMLDeclEnd,
            ],
        );

        assert_xml_generates_tokens(
            "<?xml version='1.0' encoding='UTF-8'?>",
            vec![
                Token::XMLDeclStart,
                Token::XMLVersion(XMLVersionNumber::OnePointZero),
                Token::XMLEncoding(EncName::new("UTF-8".to_string())),
                Token::XMLDeclEnd,
            ],
        );

        assert_xml_generates_tokens(
            "<?xml version='1.0'     encoding =  \"iso8859-1\"        ?>",
            vec![
                Token::XMLDeclStart,
                Token::XMLVersion(XMLVersionNumber::OnePointZero),
                Token::XMLEncoding(EncName::new("iso8859-1".to_string())),
                Token::XMLDeclEnd,
            ],
        );

        assert_xml_generates_tokens(
            "<?xml version='1.0' standalone='yes'?>",
            vec![
                Token::XMLDeclStart,
                Token::XMLVersion(XMLVersionNumber::OnePointZero),
                Token::XMLStandalone(true),
                Token::XMLDeclEnd,
            ],
        );

        assert_xml_generates_tokens(
            "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>",
            vec![
                Token::XMLDeclStart,
                Token::XMLVersion(XMLVersionNumber::OnePointZero),
                Token::XMLEncoding(EncName::new("UTF-8".to_string())),
                Token::XMLStandalone(true),
                Token::XMLDeclEnd,
            ],
        );

        assert_xml_generates_tokens(
            "<?xml version='1.0'     encoding =  \"iso8859-1\" standalone=\"no\"        ?>",
            vec![
                Token::XMLDeclStart,
                Token::XMLVersion(XMLVersionNumber::OnePointZero),
                Token::XMLEncoding(EncName::new("iso8859-1".to_string())),
                Token::XMLStandalone(false),
                Token::XMLDeclEnd,
            ],
        );
    }

    #[test]
    fn comment() {
        assert_xml_generates_tokens(
            "<?xml version='1.0'?><!--Comment-->",
            vec![
                Token::XMLDeclStart,
                Token::XMLVersion(XMLVersionNumber::OnePointZero),
                Token::XMLDeclEnd,
                Token::Comment(Comment::new("Comment".to_string())),
            ],
        );

        assert_xml_generates_tokens(
            "<?xml version='1.0'?><!--Comment--><!-- Comment 2 -->",
            vec![
                Token::XMLDeclStart,
                Token::XMLVersion(XMLVersionNumber::OnePointZero),
                Token::XMLDeclEnd,
                Token::Comment(Comment::new("Comment".to_string())),
                Token::Comment(Comment::new(" Comment 2 ".to_string())),
            ],
        );
    }

    #[test]
    fn pi() {
        assert_xml_generates_tokens(
            "<?xml version='1.0'?><?php?>",
            vec![
                Token::XMLDeclStart,
                Token::XMLVersion(XMLVersionNumber::OnePointZero),
                Token::XMLDeclEnd,
                Token::PIStart,
                Token::PITarget(PITarget::new("php".to_string())),
                Token::PIData(PIData::new(String::new())),
                Token::PIEnd,
            ],
        );

        assert_xml_generates_tokens(
            "<?xml version='1.0'?><?php echo 'hello world.'; ?>",
            vec![
                Token::XMLDeclStart,
                Token::XMLVersion(XMLVersionNumber::OnePointZero),
                Token::XMLDeclEnd,
                Token::PIStart,
                Token::PITarget(PITarget::new("php".to_string())),
                Token::PIData(PIData::new(" echo 'hello world.'; ".to_string())),
                Token::PIEnd,
            ],
        );
    }

}

// https://www.w3.org/TR/xml/#NT-S
fn is_s(input: &str) -> bool {
    match input {
        "\u{9}" | "\u{A}" | "\u{D}" | "\u{20}" => true,
        _ => false,
    }
}

//https://www.w3.org/TR/xml/#NT-document
struct Document {
    document: String,
}

// https://www.w3.org/TR/xml/#NT-Name
struct Name {
    name: String,
}
impl Name {
    fn validate(possible_name: &str) -> bool {
        let mut chars = possible_name.chars();

        match chars.next() {
            Some(first_char) => {
                if !is_name_start_char(first_char) {
                    return false;
                }
            }
            None => {
                return false;
            }
        }

        for c in chars {
            if !is_name_char(c) {
                return false;
            }
        }

        true
    }
}

// TODO? [6]
struct Names {}

// https://www.w3.org/TR/xml/#NT-Nmtoken
struct Nmtoken {
    nmtoken: String,
}
impl Nmtoken {
    fn validate(possible_nmtoken: &str) -> bool {
        if possible_nmtoken.len() < 1 {
            return false;
        }

        for c in possible_nmtoken.chars() {
            if !is_name_char(c) {
                return false;
            }
        }

        true
    }
}

// TODO [8]
struct Nmtokens {}

// TODO [9]
struct EntityValue {}

// TODO [10]
struct AttValue {}

// TODO [11]
struct SystemLiteral {}

// TODO [12]
struct PubidLiteral {}

// TODO [13]
struct PubidChar {}

// TODO [14]
struct CharData {}

// TODO [18]
struct CDSect {}

// [19]
const CD_START: &str = "<![CDATA[";

// [20]
struct CData {}

// [21]
const CD_END: &str = "]]>";

// [22]
struct Prolog {}

// [23]
//struct XMLDecl {}
//const XML_DECL_START: &str = "<?xml";
//const XML_DECL_END: &str = "?>";

// [24]
//struct VersionInfo {}
//const VERSION: &str = "version";

// [25]
//struct Eq {}

// [26]
//struct VersionNum {}

// [27]
struct Misc {}

// [28]
struct DocTypeDecl {}

// [28a]
struct DeclSep {}

// [28b]
struct IntSubSet {}

// [29]
struct MarkUpDecl {}

// [30]
struct ExtSubSet {}

// [31]
struct ExtSubSetDecl {}

// [32]
struct SDDecl {}
const STANDALONE: &str = "standalone";
const YES: &str = "yes";
const NO: &str = "no";

// [39]
struct Element {}

// [40]
struct STag {}

// [41]
struct Attribute {}

// [42]
struct ETag {}

// [43]
struct Content {}

// [44]
struct EmptyElemTag {}

// [45]
struct ElementDecl {}
// [46]
struct ContentSpec {}
// [47]
struct Children {}
// [48]
struct Cp {}
// [49]
struct Choice {}
// [50]
struct Seq {}
// [51]
struct Mixed {}
// [52]
struct AttListDecl {}
// [53]
struct AttDef {}
// [54]
struct AttType {}
// [55]
struct StringType {}
// [56]
struct TokenizedType {}
// [57]
struct EnumeratedType {}
// [58]
struct NotationType {}
// [59]
struct Enumeration {}
// [60]
struct DefaultDecl {}
// [61]
struct ConditionalSect {}
// [62]
struct IncludeSect {}
// [63]
struct IgnoreSect {}
// [64]
struct IgnoreSectContents {}
// [65]
struct Ignore {}
// [66]
struct CharRef {}
// [67]
struct Reference {}
// [68]
struct EntityRef {}
// [69]
struct PEReference {}
// [70]
struct EntityDecl {}
// [71]
struct GEDecl {}
// [72]
struct PEDecl {}
// [73]
struct EntityDef {}
// [74]
struct PEDef {}
// [75]
struct ExternalID {}
// [76]
struct NDataDecl {}
// [77]
struct TextDecl {}
// [78]
struct ExtParsedEnt {}

// [80]
struct EncodingDecl {}
const ENCODING: &str = "encoding";

// [82]
struct NotaionDecl {}
// [83]
struct PublicID {}

#[cfg(test)]
mod struct_tests {
    use super::*;

    #[test]
    fn struct_name() {
        assert!(Name::validate("legal_name"));
        assert!(Name::validate("a"));
        assert!(!Name::validate("0illegal_name"));
        assert!(!Name::validate(""));
        assert!(!Name::validate("a b"));
    }

    #[test]
    fn struct_nmtoken() {
        assert!(Nmtoken::validate("legal_nmtoken"));
        assert!(Nmtoken::validate("a"));
        assert!(Nmtoken::validate("0illegal_name_but_legal_nmtoken"));
        assert!(!Nmtoken::validate(""));
        assert!(!Nmtoken::validate("a b"));
    }

}

// https://www.w3.org/TR/xml/#NT-Char
fn is_char(c: char) -> bool {
    match c {
        '\u{9}' | '\u{A}' | '\u{D}' => true,
        '\u{20}'...'\u{D7FF}' => true,
        '\u{E000}'...'\u{FFFD}' => true,
        '\u{10000}'...'\u{10FFFF}' => true,
        _ => false,
    }
}

// TODO fn is_discouraged_char() see  https://www.w3.org/TR/xml/#NT-Char

// https://www.w3.org/TR/xml/#NT-S
fn is_whitespace(c: char) -> bool {
    match c {
        '\u{9}' | '\u{A}' | '\u{D}' | '\u{20}' => true,
        _ => false,
    }
}

// https://www.w3.org/TR/xml/#NT-NameStartChar
fn is_name_start_char(c: char) -> bool {
    match c {
        ':' | '_' => true,
        'A'...'Z' => true,
        'a'...'z' => true,
        '\u{C0}'...'\u{D6}' => true,
        '\u{D8}'...'\u{F6}' => true,
        '\u{F8}'...'\u{2FF}' => true,
        '\u{370}'...'\u{37D}' => true,
        '\u{37F}'...'\u{1FFF}' => true,
        '\u{200C}'...'\u{200D}' => true,
        '\u{2070}'...'\u{218F}' => true,
        '\u{2C00}'...'\u{2FEF}' => true,
        '\u{3001}'...'\u{D7FF}' => true,
        '\u{F900}'...'\u{FDCF}' => true,
        '\u{FDF0}'...'\u{FFFD}' => true,
        '\u{10000}'...'\u{EFFFF}' => true,
        _ => false,
    }
}

// https://www.w3.org/TR/xml/#NT-NameChar
fn is_name_char(c: char) -> bool {
    if is_name_start_char(c) {
        return true;
    }

    match c {
        '-' | '.' | '\u{B7}' => true,
        '0'...'9' => true,
        'a'...'z' => true,
        '\u{0300}'...'\u{036F}' => true,
        '\u{203F}'...'\u{2040}' => true,
        _ => false,
    }
}

// https://www.w3.org/TR/xml/#NT-EncName
fn is_enc_name_start_char(c: char) -> bool {
    match c {
        'A'...'Z' => true,
        'a'...'z' => true,
        _ => false,
    }
}

// https://www.w3.org/TR/xml/#NT-EncName
fn is_enc_name_char(c: char) -> bool {
    match c {
        'A'...'Z' => true,
        'a'...'z' => true,
        '0'...'9' => true,
        '.' | '_' | '-' => true,
        _ => false,
    }
}

fn is_comment_start_char(c: char) -> bool {
    if c == '-' {
        return false;
    } else if is_char(c) {
        return true;
    } else {
        return false;
    }
}

#[cfg(test)]
mod character_class_tests {
    use super::*;

    #[test]
    fn fn_is_char() {
        assert!(is_char('a'));
    }

    #[test]
    fn fn_is_whitespace() {
        assert!(is_whitespace('\u{9}'));
        assert!(is_whitespace('\u{a}'));
        assert!(is_whitespace('\u{d}'));
        assert!(is_whitespace('\u{20}'));

        assert!(!is_whitespace('a'));
    }

    #[test]
    fn fn_is_name_start_char() {
        assert!(is_name_start_char(':'));
        assert!(is_name_start_char('_'));
        assert!(is_name_start_char('A'));
        assert!(is_name_start_char('z'));

        assert!(!is_name_start_char('\u{20}'));
    }

}

//TODO documentation comments
